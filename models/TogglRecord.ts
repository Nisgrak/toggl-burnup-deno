// import axios from "axios";
import iro, { green, blue, red, magenta, yellow } from 'https://deno.land/x/iro/src/iro.ts';
import { clipboard } from 'https://github.com/Nisgrak/deno-clipboard/raw/fix-deno-1.0.0/mod.ts';
import { soxa } from 'https://deno.land/x/soxa/mod.ts';
import dayjs from 'https://cdn.pika.dev/dayjs@^1.8.27';
import config from "../env.config.ts";
import ApiTogglRecord from "../typings/IApiTogglRecord.ts";
import askInput from "../utils/askInput.ts";
import groupBy from "../utils/groupBy.ts";

export default class TogglRecord {
    private duration: number;
    private name: string;
    private ot: string;
    private date: any;

    constructor(record: ApiTogglRecord) {
        this.duration = Math.round(record.duration / (config.PointToMinutes * 60));
        this.name = record.description || "";
        this.date = dayjs(record.start);

        const otTag = record.tags?.find(tag => /^OT-\d+ \//.test(tag));

        if (otTag) {
            this.ot = otTag;
        } else {
            this.ot = "Comments";
        }
    }

    get Duration(): number {
        return this.duration;
    }

    get Name(): string {
        return this.name;
    }

    get Ot(): string {
        return this.ot;
    }

    get OtNum(): string {
        return this.ot.split("/")[0];
    }

    get FormatDate(): string {
        return this.date.format('DD/MM/YYYY');
    }

    get FormatName() {
        return `${this.OtNum}/ ${this.Name}`
    }

    static async getList(startDate: string, endDate: string): Promise<TogglRecord[]> {
        const result = await soxa.get(config.TogglApi + "time_entries", {
            params: {
                start_date: startDate,
                end_date: endDate
            },
            auth: {
                username: config.TogglToken,
                password: config.TogglPassword
            },
        })

        const togglRecords = result.data;

        return togglRecords.map((record: ApiTogglRecord) => new TogglRecord(record));
    }

    static async generateDay(recordList: TogglRecord[], waitToCopyComments: boolean): Promise<void> {
        const day = recordList[0].FormatDate;
        let dayPoints = 0;
        let otTasks = "";
        let comments = "";

        const recordsByName = groupBy(recordList, "FormatName");

        for (const tag of Object.keys(recordsByName)) {
            const recordsByTag = recordsByName[tag];
            const duration = recordsByTag.reduce((acc, value) => acc + value.Duration, 0);


            if (tag.startsWith("Comments/")) {
                if (comments !== "") {
                    comments += "\n";
                }

                comments += `${recordsByTag[0].Name} (${duration})`
            } else {
                if (otTasks !== "") {
                    otTasks += "\n";
                }

                otTasks += `${recordsByTag[0].FormatName} (${duration})`

            }

            dayPoints += duration;
        }

        await TogglRecord.showDay(day, dayPoints, otTasks, comments, waitToCopyComments);
    }

    static async showDay(day: string, dayPoints: number, otTasks: string, comments: string, waitToCopyComments: boolean): Promise<void> {
        console.log(iro("Día " + day, yellow))

        await clipboard.writeText(dayPoints.toString());
        console.log("%s %s", iro("Se han copiado tus puntos del día:", green), iro(dayPoints.toString(), magenta));
        await askInput(iro("Presiona cualquier enter para continuar.\n", blue));

        if (otTasks !== "") {
            await clipboard.writeText("\"" + otTasks + "\"");
            console.log("%s\n%s\n", iro("Se han copiado tus tareas del día: ", green), iro(otTasks, magenta));
            await askInput(iro("Presiona cualquier enter para continuar.\n", blue));

        } else {
            console.log(iro("No tienes ninguna tarea para el día.\n", red))
        }

        if (comments !== "") {
            await clipboard.writeText("\"" + comments + "\"");
            console.log("%s\n%s", iro("Se han copiado tus comentarios del día:", green), iro(comments, magenta));

            if (waitToCopyComments) {
                await askInput(iro("Presiona cualquier enter para continuar.\n", blue));

            }
        } else {
            console.log("%s\n", iro("No tienes ningún comentario para el día.", red))
        }
    }
}