import { readLines } from "https://deno.land/std/io/bufio.ts";

export default async (question: string): Promise<string> => {
	const buf = new Uint8Array(1024);

	// Write question to console
	await Deno.stdout.write(new TextEncoder().encode(question));

	// Read console's input into answer
	const n = <number>await Deno.stdin.read(buf);
	const answer = new TextDecoder().decode(buf.subarray(0, n));

	return answer.trim();
}