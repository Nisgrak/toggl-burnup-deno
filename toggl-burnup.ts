import iro, { bold, yellow } from 'https://deno.land/x/iro/src/iro.ts';
import args from "https://deno.land/x/args@2.0.0/wrapper.ts";
import {
	EarlyExitFlag,
	PartialOption
} from "https://deno.land/x/args@2.0.0/flag-types.ts";
import {
	Text
} from "https://deno.land/x/args@2.0.0/value-types.ts";
import {
	PARSE_FAILURE,
} from "https://deno.land/x/args@2.0.0/symbols.ts";
import standardDay from "./utils/standardDay.ts"
import groupBy from "./utils/groupBy.ts";
import TogglRecord from "./models/TogglRecord.ts";


const parser = args
	.describe("Read toggl api and copy to your clipboard in burnup format")
	.with(
		EarlyExitFlag("help", {
			describe: "Show help",
			alias: ["h"],
			exit() {
				console.log(parser.help());
				return Deno.exit();
			},
		})
	)
	.with(
		PartialOption("start", {
			type: Text,
			alias: ["s"],
			describeDefault: "today",
			describe: "start date. Default to today",
			default: ""
		})
	)
	.with(
		PartialOption("end", {
			type: Text,
			alias: ["e"],
			describeDefault: "today",

			describe: "end date. Default to today",
			default: ""
		})
	)

const res = parser.parse(Deno.args);

if (res.tag === PARSE_FAILURE) { // Alternatively, `if (res.error) {`
	console.error("Failed to parse CLI arguments");
	console.error(res.error.toString());
	Deno.exit(1);
}

let { start, end } = res.value;

let startDate = standardDay(start, 0);
let endDate = standardDay(end, 23);
console.log(iro("Fecha de inicio:", bold), iro(startDate.format('DD/MM/YYYY HH:mm'), yellow));
console.log(iro("Fecha de fin:", bold), iro(endDate.format('DD/MM/YYYY HH:mm'), yellow));


const recordList = await TogglRecord.getList(startDate.format(), endDate.format());

const listOrderedByDay = groupBy(recordList, "FormatDate");

const listOrderedByDayKeys = Object.keys(listOrderedByDay);
for (const day of Object.keys(listOrderedByDay)) {
	const recordsDay = listOrderedByDay[day];
	const hasNextDay = listOrderedByDayKeys[listOrderedByDayKeys.length - 1] !== day;
	console.log("")

	await TogglRecord.generateDay(recordsDay, hasNextDay);

	if (hasNextDay) {
		console.log("--------------")
	}
}