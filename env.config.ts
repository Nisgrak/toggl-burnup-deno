import { config } from "https://deno.land/x/dotenv/mod.ts";

export default {
    TogglApi: config().TOGGL_API ?? "",
    TogglToken: config().TOGGL_TOKEN ?? "",
    TogglPassword: config().TOGGL_PASSWORD ?? "",
    PointToMinutes: parseInt(config().POINT_TO_MINUTES ?? "", 10)
}